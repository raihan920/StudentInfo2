package com.example.raihan.studentinfo2;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* -- Received Bundle Start -- */
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            Bundle caughtBundle = extras.getBundle("sentBundle");

            String str_phone = caughtBundle.getString("mobileNumber");
            String str_email = caughtBundle.getString("emailAddress");
            String str_web_address = caughtBundle.getString("websiteAddress");

            EditText et1 = (EditText) findViewById(R.id.editText1);
            EditText et2 = (EditText) findViewById(R.id.editText2);
            EditText et3 = (EditText) findViewById(R.id.editText3);
            EditText et4 = (EditText) findViewById(R.id.editText4);

            et1.setText(str_phone);
            et2.setText(str_phone);
            et3.setText(str_email);
            et4.setText(str_web_address);
        }
        /* -- Received Bundle End -- */

        /* --- Email Button Handler Stars --- */
        Button myButtonHandler1 = (Button) findViewById(R.id.button3);
        myButtonHandler1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                sendEmail();
            }
        });
        /* --- Email Button Handler Ends --- */

        /* --- Website Button Handler Starts --- */
        Button websiteButtonHandler = (Button) findViewById(R.id.button4);
        websiteButtonHandler.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                visitWebsite();
            }
        });
        /* --- Website Button Handler Ends --- */

        /* -- Satisfactory Button Handler Starts -- */
        Button satisfactoryButtonHandler = (Button) findViewById(R.id.button5);
        satisfactoryButtonHandler.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                satisfactoryMethod();
            }
        });
        /* -- Satisfactory Button Handler Ends -- */

        /* -- Not Satisfactory Button Handler Starts -- */
        Button notSatisfactoryButtonHandler = (Button) findViewById(R.id.button6);
        notSatisfactoryButtonHandler.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                notSatisfactoryMethod();
            }
        });
        /* -- Not Satisfactory Button Handler Ends -- */

    }

    /* -- callToMobileNumber done via onClick method starts -- */
    public void callToMobileNumber(View v){
        EditText etMobileCall = (EditText) findViewById(R.id.editText1);
        String str_mobile = etMobileCall.getText().toString();

        //Toast.makeText(getApplicationContext(), str_mobile, Toast.LENGTH_LONG).show();

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + str_mobile));

        if (Build.VERSION.SDK_INT >= 23)
        {
            if ((ContextCompat.checkSelfPermission(Main2Activity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED))
            {
                ActivityCompat.requestPermissions(Main2Activity.this, new String[]{android.Manifest.permission.CALL_PHONE},1);
            }
            else
            {
                startActivity(callIntent);
            }
        }
    }
    /* -- callToMobileNumber done via onClick method ends -- */

    /* -- smsToMobileNumber done via onClick method starts -- */
    public void smsToMobileNumber(View v){
        EditText etMobileSms = (EditText) findViewById(R.id.editText2);
        String str_mobile = etMobileSms.getText().toString();

        //Toast.makeText(getApplicationContext(), str_mobile, Toast.LENGTH_LONG).show();

        Intent intentSms = new Intent( Intent.ACTION_VIEW, Uri.parse( "sms:" + str_mobile) );
        intentSms.putExtra( "sms_body", "Dear Member");
        startActivity(intentSms);
    }
    /* -- smsToMobileNumber done via onClick method ends -- */

    /* -- sendEmail done via button handler starts -- */
    public void sendEmail(){
        EditText etEmail = (EditText) findViewById(R.id.editText3); //finding id
        EditText etEmailSubject = (EditText) findViewById(R.id.editText5); //find subject
        EditText etEmailText = (EditText) findViewById(R.id.editText6); //email body

        String sentEmailTo = etEmail.getText().toString(); //converting value to string
        String sentEmailSubject = etEmailSubject.getText().toString();
        String sentEmailText = etEmailText.getText().toString();

        Intent emailHelpIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", sentEmailTo, null));
        emailHelpIntent.putExtra(Intent.EXTRA_SUBJECT, sentEmailSubject);
        emailHelpIntent.putExtra(Intent.EXTRA_TEXT, sentEmailText);
        emailHelpIntent.putExtra(Intent.EXTRA_CC, "email");
        startActivity(Intent.createChooser(emailHelpIntent, "Message to Help Desk"));
    }
    /* -- sendEmail done via button handler ends -- */

    /* -- visitWebsite done via button handler starts -- */
    public void visitWebsite(){
        EditText myWebsite = (EditText) findViewById(R.id.editText4);
        String str_website = String.valueOf(myWebsite.getText());
        Intent intentWebsite = new Intent(Intent.ACTION_VIEW,Uri.parse(str_website)); //"http://ems.ciu.edu.bd"
        startActivity(intentWebsite);
    }
    /* -- visitWebsite done via button handler ends -- */

    /* -- Satisfactory Method Done Via Button Handler Starts -- */
    public void satisfactoryMethod(){
        Intent satisfactoryReturnIntent = new Intent();
        satisfactoryReturnIntent.putExtra("satisfactory","The Result Was Satisfactory.");
        setResult(Activity.RESULT_OK,satisfactoryReturnIntent);
        finish();
    }
    /* -- Satisfactory Method Done Via Button Handler Ends -- */

    /* -- Not Satisfactory Method Done Via Button Handler Starts -- */
    public void notSatisfactoryMethod(){
        Intent NotSatisfactoryReturnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED,NotSatisfactoryReturnIntent);
        finish();
    }
    /* -- Not Satisfactory Method Done Via Button Handler Ends -- */
}
