package com.example.raihan.studentinfo2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String result = data.getStringExtra("satisfactory");
                Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Was not satisfactory/canceled", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void goToSecondaryActivity(View v){
        EditText et1 = (EditText) findViewById(R.id.editText1);
        EditText et2 = (EditText) findViewById(R.id.editText2);
        EditText et3 = (EditText) findViewById(R.id.editText3);

        String mobile = et1.getText().toString();
        String email = et2.getText().toString();
        String webAddress = et3.getText().toString();

        Bundle myBundle = new Bundle();

        myBundle.putString("mobileNumber", mobile);
        myBundle.putString("emailAddress", email);
        myBundle.putString("websiteAddress", webAddress);

        Intent myIntent = new Intent(getApplicationContext(), Main2Activity.class);
        myIntent.putExtra("sentBundle", myBundle);
        startActivityForResult(myIntent, 1);
    }
}
